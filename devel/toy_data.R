A1 <- matrix(c(0,1,0,0,0,0,
               1,0,1,0,0,0,
               0,1,0,0,0,0,
               0,0,0,0,0,0,
               0,0,0,0,0,0,
               0,0,0,0,0,0), ncol=6)
A2 <- matrix(c(0,0,0,0,0,0,
               0,0,1,0,0,0,
               0,1,0,1,1,0,
               0,0,1,0,0,0,
               0,0,1,0,0,0,
               0,0,0,0,0,0), ncol=6)
A3 <- matrix(c(0,0,0,0,0,0,
               0,0,0,0,0,0,
               0,0,0,0,0,0,
               0,0,0,0,0,0,
               0,0,0,0,0,0,
               0,0,0,0,0,0), ncol=6)
A4 <- matrix(c(0,1,0,0,0,0,
               1,0,0,1,0,0,
               0,0,0,0,0,0,
               0,1,0,0,0,0,
               0,0,0,0,0,0,
               0,0,0,0,0,0), ncol=6)
A5 <- matrix(c(0,0,0,0,0,0,
               0,0,0,0,0,0,
               0,0,0,1,0,0,
               0,0,1,0,1,0,
               0,0,0,1,0,0,
               0,0,0,0,0,0), ncol=6)
A6 <- matrix(c(0,0,0,0,0,0,
               0,0,0,0,0,0,
               0,0,0,0,0,0,
               0,0,0,0,0,0,
               0,0,0,0,0,1,
               0,0,0,0,1,0), ncol=6)


par(mfrow=c(3,2))
set.seed(2304)
Layout <- layout_in_circle(graph_from_adjacency_matrix(A1, mode = "undirected"))
plot(graph_from_adjacency_matrix(A1, "undirected"), layout=Layout)
plot(graph_from_adjacency_matrix(A2, "undirected"), layout=Layout)
plot(graph_from_adjacency_matrix(A3, "undirected"), layout=Layout)
plot(graph_from_adjacency_matrix(A4, "undirected"), layout=Layout)
plot(graph_from_adjacency_matrix(A5, "undirected"), layout=Layout)
plot(graph_from_adjacency_matrix(A6, "undirected"), layout=Layout)

As <- list(A1,A2,A3,A4,A5,A6)

library(igraph)
As <- lapply(1:20,function(x){
  set.seed(x*30)
  as.matrix(get.adjacency(erdos.renyi.game(50,0.25)))
})

Es <- lapply(seq_along(As), function(x){
  get.adjlist(graph_from_adjacency_matrix(As[[x]], "undirected"))
})


