## Test environments
* local OS X install, R 3.4.1

## R CMD check results
There were no ERRORs or WARNINGs.

## Downstream dependencies
There are currently no downstream dependencies for this package
